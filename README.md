# EDet

EDet is a deep learning algorithm / model inference **application** framework.
The goal is to provide out-of-box APIs for various deep learning tasks. It was
originally for detection task, but now also support embedding-based 
recognition task. 

ps: the letter 'E' in the name was originally means "Eric", hahaha :)

## Overview

It provides three languages interface for each algorithm:

- C++ (for Linux or MacOS only, no Window support);
- Python 3.5+;

The core of the algorithm will be implemented in modern C++ (specifically, 
C++14) with pimpl idiom. The Python interface was exposed by wrapping the core 
using pybind11.

## Features

The framework currently support the following:

- retinaface_mobilenet face detector(bounding box and 5 landmarks)
- nanodet object detector
- ultralight simplepose for skeleton detection
- facenet_mobile face recognizer

All of them can be run on realtime on Linux, MacOS or even on a phone.

## Project Structure

The folder contains this README is the root directory of the project, it contains:

- cmake/ : stores CMake related files to build (or help to build) the framework;
- depends/ : stores the dependencies using by the framework;
- examples/ : contains C++ and python examples demonstrating how to use the framework;
- images/ : contains images which will be used by the examples;
- models/ : all models needed by the framework should be put here, but in order 
  to keep the repo lightweight, we only put a text file pointing to the place where 
  you can download them;
- pylib/ : code and pybind11 source for building the Python wrapper (shared library);
- python/ : Python interface for framework user (expose the APIs inside the wrapper);
- src/ : the source of the framework;

## How to run?

You will need to download the models from the "Download" page in order to run the examples, 
unzip the file and all models should be placed inside the `models/` folder.

Then checkout the README in `examples/` folder for specific run instructions.

## How to build?

[CMake](https://cmake.org/) was used as the building system (cmake >= 3.12), 
to build the library just open a terminal and run:

```shell
cd ROOT_OF_THE_PROJECT
mkdir build
cd build
cmake ..
make
```

There are other flags to control your build:

- EDET_BUILD_EXAMPLES: default is ON, if you don't want to build examples can set
  it to OFF (note that you will need OpenCV installed to build the examples);
- EDET_PYTHON: default is OFF, if you want python api, you should turn it ON (to run python 
  examples, make sure you have the required packages installed which were listed in next section);

For example, to make a release build with python api:

```shell
# highly recommand you to create a dedicated env for openedet
conda activate edetenv

cd ROOT_OF_THE_PROJECT
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DEDET_PYTHON=ON ..
make
```

### Python Dependencies

In order to build Python API, you will need to install the following packages:

- python3.6 (tested version)
- numpy==1.19.5 (tested version)
- opencv-python== 4.5.1.48 (tested version)

To install them, highly recommend to make a clean virtual environment:

```shell
# if you are using conda
conda create --name=edetenv python=3.6
conda activate edetenv
```

```shell
# install required packages
pip install numpy==1.19.5
pip install opencv-python= 4.5.1.48 
```

## Sample Results

Some sample results from the implemented models:

![retinaface output](images/sample-results/retinaface.png)

![nanodet output](images/sample-results/nanodet.png)

![ultralight-pose output](images/sample-results/ulpose.png)
