# Examples

How to build examples, you can consult the "How to build?" section in the main 
README file.

*Note: Python examples will not build by default.*

## Command line parameters

Both C++ and Python example use the same command line parameters to
control the example behaviour. You can use `-h` to invoke the help message.

```
=====================================
            help message             
=====================================
-h: help message
-v: version message
-g: image mode flag
-o: video mode flag (default)
-s: save result flag (default false)
-i: input path
-t: output path
```

Only `-i` and `-t` can allow with a path, other parameters are just only a flag.

## Run C++ Example

```shell
cd BUILD_DIRECTORY
./examples/EXAMPLE_EXECUTABLE_NAME
```

For example, to run retinaface example using the built-in webcam:

```shell
cd BUILD_DIRECTORY
./examples/retinaface
```

To run retinaface example on an image:

```shell
cd BUILD_DIRECTORY
./examples/retinaface -g -i INPUT_IMAGE_PATH
```

- `-g`, enable the image mode, default is webcam video mode
- `-i`, specify the input image path

## Run Python Example

Since we are still in a preliminary stage of the SDK development process,
you do not add anything do the system path, so to run Python examples you
will need to do a more steps than usual.

Open a terminal, to setup a environment variable:

```shell
# root will be the directory contains the examples folder
cd ROOT_OF_THE_SDK
export PYTHONPATH=`pwd`:$PYTHONPATH
```

For example, to run retinaface example using the built-in webcam. Using the
same terminal you opened above:

```shell
# you have to issue the command in the project root folder
python examples/retinaface.py
```

To run retinaface example on an image, in the same terminal:

```shell
# you have to issue the command in the ROOT folder
python examples/retinaface.py -g -i INPUT_IMAGE_PATH
```

*Note: every time you close the terminal, you have to repeat the step to set
the environment variable.*
