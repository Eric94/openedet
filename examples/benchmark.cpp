//
// Created by Haotao Lai on 2021-02-10.
//

#include "exhelper.h"
#include "edet.h"

#include <cstdio>
#include <opencv2/core/core.hpp>

int main(int argc, char** argv)
{
    cv::Mat image = cv::imread(std::string(EDET_IMAGES_DIR_PATH) + "person.jpg");

    edet::Context context;
    auto& factory = context.get_factory();

    std::unordered_map<std::string, edet::DetectorSP> detmap{
            {"retinaface_ncnn", factory->create_detector(edet::DetectorEnum::FACE_RETINA_NCNN)},
            {"nanodet_ncnn", factory->create_detector(edet::DetectorEnum::OBJ_NANODET_NCNN)},
            {"lightpose_ncnn", factory->create_detector(edet::DetectorEnum::OBJ_LIGHTPOSE_NCNN)},
    };

    printf("Benchmark ...\n");
    for (auto & it : detmap)
    {
        auto result = benchmark_on_image(it.second.get(), image);
        printf("%s, min: %f, max: %f, avg: %f\n",
                it.first.c_str(), result->min, result->max, result->avg);
    }
}