//
// Created by haotaolai on 2021-01-23.
//

#ifndef EDET_EXHELPER_HPP
#define EDET_EXHELPER_HPP

#include "detector.h"
#include "recognizer.h"
#include "version.h"

#include <memory>
#include <cstdio>
#include <string>
#include <string_view>

// h: help message
// v: version message
// g: image mode flag
// o: video mode flag
// s: save result flag (default false)
// i: input path
// t: output path

struct CmdArgs
{
	bool img_mode;
	bool vdo_mode;
	bool should_save;
	std::string in_path;
	std::string out_path;
	std::string version;

	CmdArgs()
		: img_mode{false}
		, vdo_mode{true}
		, should_save{false}
		, in_path{}
		, out_path{}
		, version{edet::get_version_str()}
	{}
};

class CmdArgParser {
private:
	inline static const std::string emptyString = "";

public:
	CmdArgParser(int argc, char* argv[])
		: options{}
		, lastQuery{emptyString}
		, isValidCache{false}
	{
		for (size_t i = 1; i < argc; i++)
		{
			options.emplace_back(argv[i]);
		}
	}

	bool hasCmdOption(std::string_view lookingfor)
	{
		std::vector<std::string>::const_iterator it;
		it = std::find(options.begin(), options.end(), lookingfor);
		if (it == options.end())
		{
			isValidCache = false;
			lastQuery = emptyString;
			return false;
		}
		else
		{
			isValidCache = true;
			lastQuery = *(++it);
			return true;
		}
	}

	std::string getCmdOption(std::string_view lookingfor)
	{
		if (isValidCache)
		{
			std::string tmp = lastQuery;
			lastQuery = emptyString;
			isValidCache = false;
			return tmp;
		}
		else
		{
			std::vector<std::string>::const_iterator it;
			it = std::find(options.begin(), options.end(), lookingfor);
			if (it != options.end() && ++it != options.end())
			{
				return *it;
			}
			else
			{
				return emptyString;
			}
		}
	}
private:
	std::vector<std::string> options;
	std::string lastQuery;
	bool isValidCache;
};

void print_helpmsg()
{
	printf("=====================================\n"
		"            help message             \n"
		"=====================================\n"
		"-h: help message\n"
		"-v: version message\n"
		"-g: image mode flag\n"
		"-o: video mode flag (default)\n"
		"-s: save result flag (default false)\n"
		"-i: input path\n"
		"-t: output path\n\n");
}

std::unique_ptr<CmdArgs> parse_cmd(int argc, char* argv[])
{
	// default
	auto pCmdArgs = std::make_unique<CmdArgs>();

	CmdArgParser parser(argc, argv);

	if (parser.hasCmdOption("-h"))
	{
		print_helpmsg();
		exit(0);
	}
	if (parser.hasCmdOption("-v"))
	{
		printf("[cmd] edet version: %s\n\n", pCmdArgs->version.c_str());
		exit(0);
	}
	if (parser.hasCmdOption("-g"))
	{
		pCmdArgs->img_mode = true;
		pCmdArgs->vdo_mode = false;
	}
	if (parser.hasCmdOption("-o"))
	{
		pCmdArgs->vdo_mode = true;
		pCmdArgs->img_mode = false;
	}
	if (parser.hasCmdOption("-i"))
	{
		pCmdArgs->in_path = parser.getCmdOption("-i");
	}
	if (parser.hasCmdOption("-s"))
	{
		pCmdArgs->should_save = true;
	}
	if (parser.hasCmdOption("-t"))
	{
		pCmdArgs->out_path = parser.getCmdOption("-t");
	}

	// output the cmd parsing result
	if (pCmdArgs->vdo_mode)
	{
		printf("[cmd] work on video mode\n");
	}
	else if (pCmdArgs->img_mode)
	{
		printf("[cmd] work on image mode\n");
	}

	if (pCmdArgs->in_path.empty() && pCmdArgs->img_mode)
	{
		printf("[cmd] input path is missing\n");
		exit(-1);
	}
	else
	{
		if (pCmdArgs->in_path.empty())
		{
			printf("[cmd] video mode has no input path specified, will use the live camera");
		}
		else
		{
			printf("[cmd] input path: %s\n", pCmdArgs->in_path.c_str());
		}
	}

	if (pCmdArgs->should_save && pCmdArgs->out_path.empty())
	{
		printf("[cmd] save result enable, result will save to: %s\n", pCmdArgs->out_path.c_str());
	}
	printf("\n");
	return std::move(pCmdArgs);
}

const int ESC_KEY = 27;
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <numeric>
#include "opencv2/imgcodecs.hpp"

cv::Mat read_img(const char* imgpath)
{
	cv::Mat image = cv::imread(imgpath, 1);
	if (image.empty())
	{
		fprintf(stderr, "[exhelper] cv::imread %s failed\n", imgpath);
		exit(-1);
	}
	return image;
}

void run_image(edet::Detector* pdet,
	cv::Mat& image,
	const std::function<void(const edet::BboxSP& box, cv::Mat& image)>& draw)
{
	cv::Mat rgb;
	cv::cvtColor(image, rgb, cv::COLOR_BGR2RGB);

	const std::vector<edet::BboxSP>& bbs = pdet->detect(rgb.data, rgb.cols, rgb.rows);

	for (auto& box : bbs)
		draw(box, image);
	cv::imshow("image", image);
	cv::waitKey();
}

void run_video(CmdArgs* args,
	edet::Detector* pdet,
	const std::function<void(const edet::BboxSP& box, cv::Mat& image)>& draw)
{
	cv::VideoCapture cap;
	if (args->vdo_mode && args->in_path.empty())
		cap.open(0);
	else
		cap.open(args->in_path);

	if (!cap.isOpened())
	{
		printf("[exhelper] error while opening video stream or file");
		exit(-1);
	}

	int key = 0;
	while (key != ESC_KEY)
	{
		cv::Mat frame;
		cap >> frame;
		if (frame.empty()) break;

		cv::Mat rgb;
		cv::cvtColor(frame, rgb, cv::COLOR_BGR2RGB);

		const std::vector<edet::BboxSP>& bbs = pdet->detect(rgb.data, rgb.cols, rgb.rows);

		for (auto& box : bbs)
			draw(box, frame);

		cv::imshow("video", frame);
		key = cv::waitKey(20);
	}

	cv::destroyAllWindows();
}

struct BenchmarkRes
{
	float min;
	float max;
	float avg;
};

const int WARM_UP = 5;
const int BENCHMARK_LOOP = 20;

std::unique_ptr<BenchmarkRes> benchmark_on_image(edet::Detector* pdet, cv::Mat& image)
{
	cv::Mat rgb;
	cv::cvtColor(image, rgb, cv::COLOR_BGR2RGB);

	for (int i = 0; i < WARM_UP; ++i)
		pdet->detect(rgb.data, rgb.cols, rgb.rows);

	std::vector<float> times(BENCHMARK_LOOP);
	for (int j = 0; j < BENCHMARK_LOOP; ++j)
	{
		auto t1 = std::chrono::high_resolution_clock::now();
		pdet->detect(rgb.data, rgb.cols, rgb.rows);
		auto t2 = std::chrono::high_resolution_clock::now();
		times[j] = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
	}

	float min = FLT_MAX;
	float max = 0;
	float avg = 0;
	float sum = 0;
	for (float t : times)
	{
		if (t < min) min = t;
		if (t > max) max = t;
		sum += t;
	}
	avg = sum / times.size();

	std::unique_ptr<BenchmarkRes> res = MAKE_UP(BenchmarkRes)();
	res->min = min;
	res->max = max;
	res->avg = avg;
	return res;
}

#include <filesystem>
#include <sys/stat.h>
#include <unordered_map>

const int DIR_ONLY = 0x01;
const int FILE_ONLY = 0x02;
const int BOTH_DIR_FILE = 0x03;

std::vector<std::string> listdir(const std::string& dirpath,
	int lookingfor = BOTH_DIR_FILE)
{
	namespace fs = std::filesystem;

	// lookingfor 0x01: only dirs
	// lookingfor 0x10: only files
	// lookingfor 0x11: all dirs and files
	// result will always NOT contains "." and ".."

	std::vector<std::string> res;

	fs::path p(dirpath);
	if (!fs::exists(p))
	{
		printf("[exhelper] cannot open directory: %s", dirpath.c_str());
		exit(-1);
	}

	for (const auto& entry : fs::directory_iterator(p))
	{
		if (entry.path().string() == "." || entry.path().string() == "..")
		{
			continue;
		}

		if (lookingfor == BOTH_DIR_FILE)
		{
			res.push_back(entry.path().string());
		}
		else if (lookingfor == DIR_ONLY && entry.is_directory())
		{
			res.push_back(entry.path().string());
		}
		else if (lookingfor == FILE_ONLY && entry.is_regular_file())
		{
			res.push_back(entry.path().string());
		}
	}

	return res;
}

std::unordered_map<std::string, edet::EmbeddingSP>
create_recog_db(const std::string& db_path, edet::Detector* pdet, edet::Recognizer* precog)
{
	// dp_path contains a list of directory which the their name will be the id
	// and each directory contain an target image of that id
	// db_path:
	//   - Trump
	//      - xxx.jgp
	//   - Biden
	//      - yyy.jgp
	//   - Obama
	//      - zzz.jpg
	namespace fs = std::filesystem;

	std::unordered_map<std::string, edet::EmbeddingSP> map;
	const std::vector<std::string>& subdirs = listdir(db_path, DIR_ONLY);
	for (auto& fullpath : subdirs) {
		// subdir is the id of each subdirectory
		auto files = listdir(fullpath, FILE_ONLY);
		if (!files.empty())
		{
			fs::path p(files[0]);
			auto pstring = p.parent_path().string();
			std::string subdir = pstring.substr(pstring.find_last_of("/\\") + 1);
			cv::Mat rgb;
			cv::Mat img = cv::imread(files[0]);
			cv::cvtColor(img, rgb, cv::COLOR_BGR2RGB);
			edet::EmbeddingSP embedding = precog->compute(rgb.data, rgb.cols, rgb.rows);
			map.emplace(subdir, embedding);
		}
	}

	printf("[exhelper] create a recognition database with %lu identities\n\n", map.size());
	return map;
}

#endif //EDET_EXHELPER_HPP
