//
// Created by haotaolai on 2021-01-25.
//
#include "edet.h"
#include "exhelper.h"


int main(int argc, char** argv)
{
    edet::Context context;
    auto& factory = context.get_factory();

    edet::DetectorUP pdet = factory->create_detector(edet::DetectorEnum::FACE_RETINA_NCNN);
    edet::RecognizerUP precog = factory->create_recognizer(edet::RecognizerEnum::FACENET_MOBILE_NCNN);
    auto db = create_recog_db(std::string(EDET_IMAGES_DIR_PATH) + "recogdb/", pdet.get(), precog.get());

    std::vector<std::string> names{
        "trump2.png",
        "biden2.png",
    };

    for (const auto& imgname : names)
    {
        cv::Mat rgb;
        cv::Mat bgr = cv::imread(std::string(EDET_IMAGES_DIR_PATH) + imgname);

        cv::cvtColor(bgr, rgb, cv::COLOR_BGR2RGB);
        const edet::EmbeddingSP& e = precog->compute(rgb.data, rgb.cols, rgb.rows);

        for (auto& entry : db)
        {
            float dist = edet::Embedding::calc_similarity(e, entry.second);
            printf("[facenetmobile] %s vs. %s similarity -> %f\n",
                   imgname.c_str(), entry.first.c_str(), dist);
        }

        printf("\n");
    }

    return 0;
}