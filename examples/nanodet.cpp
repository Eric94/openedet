//
// Created by haotaolai on 2020-11-13.
//

#include <cstdio>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>

#include "exhelper.h"
#include "edet.h"

std::vector<std::string> labels{
    "person", "bicycle", "car", "motorcycle", "airplane", "bus", "train", "truck",
    "boat", "traffic light", "fire hydrant", "stop sign", "parking meter", "bench",
    "bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra",
    "giraffe", "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee",
    "skis", "snowboard", "sports ball", "kite", "baseball bat", "baseball glove",
    "skateboard", "surfboard", "tennis racket", "bottle", "wine glass", "cup",
    "fork", "knife", "spoon", "bowl", "banana", "apple", "sandwich", "orange",
    "broccoli", "carrot", "hot dog", "pizza", "donut", "cake", "chair", "couch",
    "potted plant", "bed", "dining table", "toilet", "tv", "laptop", "mouse",
    "remote", "keyboard", "cell phone", "microwave", "oven", "toaster", "sink",
    "refrigerator", "book", "clock", "vase", "scissors", "teddy bear",
    "hair drier", "toothbrush"};

void draw(const edet::BboxSP& box, cv::Mat& image)
{
    fprintf(stdout, "class: %s, box: %f, %f, %f, %f\n",
            labels[box->label()].c_str(), box->x1(), box->y1(), box->x2(), box->y2());
    cv::rectangle(image, cv::Point(box->x1(), box->y1()), cv::Point(box->x2(), box->y2()),
                  cv::Scalar(0, 255, 0), 2);
    cv::putText(image, labels[box->label()], cv::Point(box->x1() + 10, box->y1() + 20),
                cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 0, 255));
}

int main(int argc, char** argv)
{
    std::unique_ptr<CmdArgs> args = parse_cmd(argc, argv);

    edet::Context context;
    auto& factory = context.get_factory();
    edet::DetectorUP ptr = factory->create_detector(edet::DetectorEnum::OBJ_NANODET_NCNN);

    if (args->img_mode)
    {
        cv::Mat image = read_img(args->in_path.c_str());
        run_image(ptr.get(), image, &draw);
    }
    else if (args->vdo_mode)
    {
        run_video(args.get(), ptr.get(), &draw);
    }

    return 0;
}