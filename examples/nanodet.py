from python.ncnn_nanodet import NanodetNcnnObjDetector
import cv2
import argparse


labels = [
    "person", "bicycle", "car", "motorcycle", "airplane", "bus", "train", "truck",
    "boat", "traffic light", "fire hydrant", "stop sign", "parking meter", "bench",
    "bird", "cat", "dog", "horse", "sheep", "cow", "elephant", "bear", "zebra",
    "giraffe", "backpack", "umbrella", "handbag", "tie", "suitcase", "frisbee",
    "skis", "snowboard", "sports ball", "kite", "baseball bat", "baseball glove",
    "skateboard", "surfboard", "tennis racket", "bottle", "wine glass", "cup",
    "fork", "knife", "spoon", "bowl", "banana", "apple", "sandwich", "orange",
    "broccoli", "carrot", "hot dog", "pizza", "donut", "cake", "chair", "couch",
    "potted plant", "bed", "dining table", "toilet", "tv", "laptop", "mouse",
    "remote", "keyboard", "cell phone", "microwave", "oven", "toaster", "sink",
    "refrigerator", "book", "clock", "vase", "scissors", "teddy bear",
    "hair drier", "toothbrush"]


def draw(bbox, image):
    cv2.rectangle(image, (bbox.x1, bbox.y1), (bbox.x2, bbox.y2), (0, 255, 0), thickness=2)
    cv2.putText(image, labels[bbox.label], (bbox.x1 + 10, bbox.y1 + 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='edetpy nanodet example')
    parser.add_argument('-g', action='store_true', help='enable image mode')
    parser.add_argument('-o', action='store_true', help='enable video mode')
    parser.add_argument('-s', action='store_false', help='save the program output (video or image)')
    parser.add_argument('-i', action='store', dest='i', default=None,
                        help='input source (video or image path)')
    parser.add_argument('-t', action='store', dest='t', default=None,
                        help='output destination')
    args = parser.parse_args()

    detector = NanodetNcnnObjDetector()

    if args.g:
        print('[nanodet-py] image mode')
        img = cv2.imread(args.i)
        rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        bbs = detector.detect(rgb)

        for box in bbs:
            draw(box, img)

        cv2.imshow('image', img)
        cv2.waitKey()

    else:
        print('[nanodet-py] video mode')
        is_livecam = False
        if args.i:
            cap = cv2.VideoCapture(args.i)
        else:
            print('[nanodet-py] using live webcam')
            cap = cv2.VideoCapture(0)
            is_livecam = True

        key = 0
        while key != 27:
            rval, frame = cap.read()
            if not rval: break
            if is_livecam:
                frame = cv2.flip(frame, 1)

            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            bbs = detector.detect(rgb)

            for box in bbs:
                draw(box, frame)

            cv2.imshow('video', frame)
            key = cv2.waitKey(20)

    cv2.destroyAllWindows()
