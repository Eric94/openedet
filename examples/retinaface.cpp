//
// Created by haotaolai on 2020-11-13.
//

#include <cstdio>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>

#include "exhelper.h"
#include "edet.h"

void draw(const edet::BboxSP& box, cv::Mat& image)
{
    printf("[retinaface] box: %f, %f, %f, %f\n", box->x1(), box->y1(), box->x2(), box->y2());
    cv::rectangle(image, cv::Point(box->x1(), box->y1()), cv::Point(box->x2(), box->y2()), cv::Scalar(0, 255, 0), 2);

    if (box->hasLandmark())
    {
        for (auto& lm : box->getLandmarks())
            cv::circle(image, cv::Point(lm.x, lm.y), 2, cv::Scalar(0, 255, 255), -1);
    }
}

int main(int argc, char** argv)
{
    std::unique_ptr<CmdArgs> args = parse_cmd(argc, argv);

    edet::Context context;
    auto& factory = context.get_factory();
    edet::DetectorUP ptr = factory->create_detector(edet::DetectorEnum::FACE_RETINA_NCNN);

    if (args->img_mode)
    {
        cv::Mat image = read_img(args->in_path.c_str());
        run_image(ptr.get(), image, &draw);
    }
    else if (args->vdo_mode)
    {
        run_video(args.get(), ptr.get(), &draw);
    }

    return 0;
}
