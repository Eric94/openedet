from python.ncnn_retinaface import RetinaNcnnFaceDetector
import cv2
import argparse


def draw(box, image):
    cv2.rectangle(image, (box.x1, box.y1), (box.x2, box.y2), (0, 255, 0), thickness=2)
    if box.lms:
        for p in box.lms:
            cv2.circle(image, (int(p.x), int(p.y)), 2, (0, 255, 255), -1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='edetpy retinaface example')
    parser.add_argument('-g', action='store_true', help='enable image mode')
    parser.add_argument('-o', action='store_true', help='enable video mode')
    parser.add_argument('-s', action='store_false', help='save the program output (video or image)')
    parser.add_argument('-i', action='store', dest='i', default=None,
                        help='input source (video or image path)')
    parser.add_argument('-t', action='store', dest='t', default=None,
                        help='output destination')
    args = parser.parse_args()

    detector = RetinaNcnnFaceDetector()

    if args.g:
        print('[retinaface-py] image mode:', args.i)
        img = cv2.imread(args.i)
        rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        bbs = detector.detect(rgb)

        for box in bbs:
            draw(box, img)

        cv2.imshow('image', img)
        cv2.waitKey()

    else:
        print('[retinaface-py] video mode')
        is_livecam = False
        if args.i:
            cap = cv2.VideoCapture(args.i)
        else:
            print('[retinaface-py] using live webcam')
            cap = cv2.VideoCapture(0)
            is_livecam = True

        key = 0
        while key != 27:
            rval, frame = cap.read()
            if not rval: break
            if is_livecam:
                frame = cv2.flip(frame, 1)

            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            bbs = detector.detect(rgb)

            for box in bbs:
                draw(box, frame)

            cv2.imshow('video', frame)
            key = cv2.waitKey(20)

    cv2.destroyAllWindows()
