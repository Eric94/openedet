//
// Created by haotaolai on 2020-11-13.
//

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>

#include "exhelper.h"
#include "edet.h"

static const int joint_pairs[16][2] = {
    {0, 1}, {1, 3}, {0, 2}, {2, 4}, {5, 6}, {5, 7}, {7, 9}, {6, 8}, {8, 10},
    {5, 11}, {6, 12}, {11, 12}, {11, 13}, {12, 14}, {13, 15}, {14, 16}};

void draw(const edet::BboxSP& box, cv::Mat& image)
{
    if (box->isSktBox())
    {
        const std::vector<edet::Point2f>& keypoints = box->getLandmarks();
        const std::vector<float>& probs = box->getLmProbs();

        // draw bone
        for (auto joint_pair : joint_pairs)
        {
            const edet::Point2f& p1 = keypoints[joint_pair[0]];
            const edet::Point2f& p2 = keypoints[joint_pair[1]];
            if (probs[joint_pair[0]] < 0.2f || probs[joint_pair[1]] < 0.2f)
                continue;
            cv::line(image, cv::Point(p1.x, p1.y), cv::Point(p2.x, p2.y),
                     cv::Scalar(255, 0, 0), 2);
        }
        // draw joint
        for (int i = 0; i < keypoints.size(); i++)
        {
            auto& p = keypoints[i];
            if (probs[i] < 0.2f) continue;
            cv::circle(image, cv::Point(p.x, p.y), 3,
                       cv::Scalar(0, 255, 0), -1);
        }
    }
}

int main(int argc, char** argv)
{
    std::unique_ptr<CmdArgs> args = parse_cmd(argc, argv);

    edet::Context context;
    auto& factory = context.get_factory();
    edet::DetectorUP ptr = factory->create_detector(edet::DetectorEnum::OBJ_LIGHTPOSE_NCNN);

    if (args->img_mode)
    {
        cv::Mat image = read_img(args->in_path.c_str());
        run_image(ptr.get(), image, &draw);
    }
    else if (args->vdo_mode)
    {
        run_video(args.get(), ptr.get(), &draw);
    }

    return 0;
}
