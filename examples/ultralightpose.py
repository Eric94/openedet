from python.ncnn_ultralightpose import UltralightNcnnPoseDetector
import cv2
import argparse


joint_pairs = [
    (0, 1), (1, 3), (0, 2), (2, 4), (5, 6), (5, 7), (7, 9), (6, 8), (8, 10),
    (5, 11), (6, 12), (11, 12), (11, 13), (12, 14), (13, 15), (14, 16)]


def draw(box, image):
    if box.lms:
        for idx1, idx2 in joint_pairs:
            p1 = box.lms[idx1]
            p2 = box.lms[idx2]
            if box.lmprobs[idx1] < 0.2 or box.lmprobs[idx2] < 0.2:
                continue
            cv2.line(image, (int(p1.x), int(p1.y)), (int(p2.x), int(p2.y)), (255, 0, 0), 2)

        for i in range(len(box.lms)):
            if box.lmprobs[i] < 0.2:
                continue
            p = box.lms[i]
            cv2.circle(image, (int(p.x), int(p.y)), 3, (0, 255, 0), -1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='edetpy ultralightpose example')
    parser.add_argument('-g', action='store_true', help='enable image mode')
    parser.add_argument('-o', action='store_true', help='enable video mode')
    parser.add_argument('-s', action='store_false', help='save the program output (video or image)')
    parser.add_argument('-i', action='store', dest='i', default=None,
                        help='input source (video or image path)')
    parser.add_argument('-t', action='store', dest='t', default=None,
                        help='output destination')
    args = parser.parse_args()

    detector = UltralightNcnnPoseDetector()

    if args.g:
        print('[ultralightpose-py] image mode')
        img = cv2.imread(args.i)
        rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        bbs = detector.detect(rgb)

        for box in bbs:
            draw(box, img)

        cv2.imshow('image', img)
        cv2.waitKey()

    else:
        print('[ultralightpose-py] video mode')
        is_livecam = False
        if args.i:
            cap = cv2.VideoCapture(args.i)
        else:
            print('[ultralightpose-py] using live webcam')
            cap = cv2.VideoCapture(0)
            is_livecam = True

        key = 0
        while key != 27:
            rval, frame = cap.read()
            if not rval: break
            if is_livecam:
                frame = cv2.flip(frame, 1)

            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            bbs = detector.detect(rgb)

            for box in bbs:
                draw(box, frame)

            cv2.imshow('video', frame)
            key = cv2.waitKey(20)

    cv2.destroyAllWindows()
