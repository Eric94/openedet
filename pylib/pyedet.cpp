//
// Created by haotaolai on 2021-01-29.
//

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <opencv2/core.hpp>

#include "ndarray_converter.hpp"
#include "bbox.h"
#include "config.h"
#include "factory.h"
#include "type.h"
#include "ncnn_face_retina.h"
#include "ncnn_obj_nanodet.h"
#include "ncnn_obj_lightpose.h"


class DetectorOcv {
public:
    ~DetectorOcv() = default;;

    std::vector<edet::BboxSP> detect(cv::Mat& rgb, int width, int height) const
    {
        return p->detect(rgb.data, width, height);
    }

    explicit DetectorOcv(edet::DetectorEnum denum)
    {
        switch (denum)
        {
        case edet::DetectorEnum::FACE_RETINA_NCNN:
            p = MAKE_UP(edet::Detector)(MAKE_UP(edet::FaceRetinaNcnn)());
            break;
        case edet::DetectorEnum::OBJ_NANODET_NCNN:
            p = MAKE_UP(edet::Detector)(MAKE_UP(edet::ObjNanodetNcnn)());
            break;
        case edet::DetectorEnum::OBJ_LIGHTPOSE_NCNN:
            p = MAKE_UP(edet::Detector)(MAKE_UP(edet::ObjUltralightPoseNcnn)());
            break;
        }
    }

private:
    edet::DetectorUP p;
};


namespace py = pybind11;

PYBIND11_MODULE(pyedet, m) {
    NDArrayConverter::init_numpy();

    py::class_<edet::Point2f>(m, "Point2f")
        .def(py::init<float, float>())
        .def_readwrite("x", &edet::Point2f::x)
        .def_readwrite("y", &edet::Point2f::y)
    ;

    py::class_<edet::Bbox, std::shared_ptr<edet::Bbox>>(m, "Bbox")
        .def(py::init<float, float, float, float, float>())
        .def(py::init<float, float, float, float, int, float>())
        .def_property("x1", &edet::Bbox::x1, &edet::Bbox::setX1)
        .def_property("y1", &edet::Bbox::y1, &edet::Bbox::setY1)
        .def_property("x2", &edet::Bbox::x2, &edet::Bbox::setX2)
        .def_property("y2", &edet::Bbox::y2, &edet::Bbox::setY2)
        .def_property_readonly("id", &edet::Bbox::label)
        .def_property_readonly("prob", &edet::Bbox::prob)
        .def("getLandmarks", &edet::Bbox::getLandmarks)
        .def("hasLandmark", &edet::Bbox::hasLandmark)
        .def("isSktBox", &edet::Bbox::isSktBox)
        .def("getLandmarkProbs", &edet::Bbox::getLmProbs)
    ;

    py::enum_<edet::DetectorEnum>(m, "DetectorEnum")
        .value("FACE_RETINA_NCNN", edet::DetectorEnum::FACE_RETINA_NCNN)
        .value("OBJ_NANODET_NCNN", edet::DetectorEnum::OBJ_NANODET_NCNN)
        .value("OBJ_LIGHTPOSE_NCNN", edet::DetectorEnum::OBJ_LIGHTPOSE_NCNN)
        .export_values()
    ;

    py::enum_<edet::RecognizerEnum>(m, "RecognizerEnum")
        .value("FACENET_MOBILE_NCNN", edet::RecognizerEnum::FACENET_MOBILE_NCNN)
        .export_values()
    ;

    py::class_<DetectorOcv>(m, "DetectorOcv")
        .def(py::init<edet::DetectorEnum>())
        .def("detect", &DetectorOcv::detect, py::return_value_policy::move)
    ;


#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}
