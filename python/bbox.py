import numpy as np


class Bbox:
    def __init__(self, x1, y1, x2, y2, id, prob):
        self._data = np.array([x1, y1, x2, y2])
        self.label = id
        self.prob = prob
        self.lms = None
        self.lmprobs = None

    def __iter__(self):
        return iter(self._data)

    def __repr__(self):
        return ('Bbox{P1({}, {}), P2({}, {})}'
                .format(self._data[0], self._data[1], self._data[2], self._data[3]))

    def __str__(self):
        return self._data.__str__()

    @property
    def x1(self):
        return self._data[0]

    @property
    def y1(self):
        return self._data[1]

    @property
    def x2(self):
        return self._data[2]

    @property
    def y2(self):
        return self._data[3]

    @property
    def height(self):
        return self.y2 - self.y1

    @property
    def width(self):
        return self.x2 - self.x1

    @property
    def area(self):
        return self.height * self.width

    @staticmethod
    def iou(b1, b2):
        # determine the (x, y)-coordinates of the intersection rectangle
        x_a = max(b1.x1, b2.x1)
        y_a = max(b1.y1, b2.y1)
        x_b = min(b1.x2, b2.x2)
        y_b = min(b1.y2, b2.y2)
        # compute the area of intersection rectangle
        inter_area = max(0, x_b - x_a + 1) * max(0, y_b - y_a + 1)
        # compute the area of both the prediction and ground-truth
        # rectangles
        b1_area = (b1.x2 - b1.x1 + 1) * (b1.y2 - b1.y1 + 1)
        b2_area = (b2.x2 - b2.x1 + 1) * (b2.y2 - b2.y1 + 1)
        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the intersection area
        iou = inter_area / float(b1_area + b2_area - inter_area)
        # return the intersection over union value
        return iou

    @staticmethod
    def no_overlap(bbs, iou_threshold=0.3):
        if not bbs or len(bbs) == 1:
            return bbs

        bbs.sort(key=lambda box: box.area, reverse=True)
        first = bbs[0]
        res = [bbs[i] for i in range(1, len(bbs)) if Bbox.iou(first, bbs[i]) < iou_threshold]
        res.append(first)
        return res
