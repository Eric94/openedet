import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'build', 'pylib'))
try:
    import pyedet
except ImportError as e:
    print('cannot import pyedet:', e)
    print(sys.path)
from python.bbox import Bbox


class NanodetNcnnObjDetector:
    def __init__(self):
        self.detector = pyedet.DetectorOcv(pyedet.DetectorEnum.OBJ_NANODET_NCNN)

    def detect(self, rgb):
        bbs = []
        objs = self.detector.detect(rgb, rgb.shape[1], rgb.shape[0])
        for box in objs:
            x1, y1, x2, y2 = int(box.x1), int(box.y1), int(box.x2), int(box.y2)
            bbs.append(Bbox(x1, y1, x2, y2, box.id, box.prob))

        return bbs
