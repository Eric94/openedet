import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'build', 'pylib'))
try:
    import pyedet
except ImportError as e:
    print('cannot import pyedet:', e)
    print(sys.path)
from python.bbox import Bbox


class RetinaNcnnFaceDetector:
    def __init__(self):
        self.detector = pyedet.DetectorOcv(pyedet.DetectorEnum.FACE_RETINA_NCNN)

    def detect(self, rgb):
        bbs = []
        faces = self.detector.detect(rgb, rgb.shape[1], rgb.shape[0])
        for face in faces:
            x1, y1, x2, y2 = int(face.x1), int(face.y1), int(face.x2), int(face.y2)
            b = Bbox(x1, y1, x2, y2, face.id, face.prob)
            bbs.append(b)

            if face.hasLandmark():
                b.lms = face.getLandmarks()
            if face.isSktBox():
                b.lmprobs = face.getLandmarkProbs()

        return bbs
