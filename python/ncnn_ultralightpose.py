import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'build', 'pylib'))
try:
    import pyedet
except ImportError as e:
    print('cannot import pyedet:', e)
    print(sys.path)
from python.bbox import Bbox


class UltralightNcnnPoseDetector:
    def __init__(self):
        self.detector = pyedet.DetectorOcv(pyedet.DetectorEnum.OBJ_LIGHTPOSE_NCNN)

    def detect(self, rgb):
        bbs = []
        objs = self.detector.detect(rgb, rgb.shape[1], rgb.shape[0])
        for box in objs:
            x1, y1, x2, y2 = int(box.x1), int(box.y1), int(box.x2), int(box.y2)
            b = Bbox(x1, y1, x2, y2, box.id, box.prob)
            bbs.append(b)
            if box.hasLandmark():
                b.lms = box.getLandmarks()
            if box.isSktBox():
                b.lmprobs = box.getLandmarkProbs()

        return bbs
