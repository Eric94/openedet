//
// Created by haotaolai on 2021-02-04.
//

#ifndef EDET_CONFIG_CONST_H
#define EDET_CONFIG_CONST_H

#include <string>

namespace edet {
namespace config_const {

// ********************* model file name ********************************* //
const std::string retinface_fn{"retinaface-mnet.25"};
const std::string nanodet_fn{"nanodet_m"};
const std::string ultralightpose_fn{"ultralight-pose"};

const std::string mobilefacenet{"mobilefacenet"};

} // namespace config_const
} // namespace edet

#endif //EDET_CONFIG_CONST_H
