//
// Created by haotaolai on 2021-01-10.
//

#ifndef EDET_DETHELPER_H
#define EDET_DETHELPER_H

#include <memory>
#include <vector>
#include <ncnn/mat.h>
#include "bbox.h"

namespace edet {
class Helper
{
public:
    static void qsort_descent_inplace(std::vector<BboxSP>& bbs, int left, int right);

    static void qsort_descent_inplace(std::vector<BboxSP>& bbs);

    static void nms_sorted_bboxes(const std::vector<BboxSP>& bbs,
                                  std::vector<int>& picked,
                                  float nms_threshold);

    // copy from src/layer/proposal.cpp
    static ncnn::Mat generate_anchors(int base_size,
                                      const ncnn::Mat& ratios,
                                      const ncnn::Mat& scales);
};

} // namespace edet
#endif //EDET_DETHELPER_H
