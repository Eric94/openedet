//
// Created by haotaolai on 2021-01-10.
//

#ifndef EDET_LOADER_PATH_H
#define EDET_LOADER_PATH_H

#include <memory>
#include <string>
#include "loader.h"

namespace edet {

typedef std::unique_ptr<ncnn::Net> NcnnNetUP;

struct PathLoaderSource
{
    std::string param_path;
    std::string bin_path;
};

class PathLoader : public NcnnNetLoader
{
public:
    PathLoader(const std::string& param_fn, const std::string& bin_fn)
        : source(MAKE_UP(PathLoaderSource)())
    {
        source->param_path = EDET_MODEL_DIR + param_fn;
        source->bin_path = EDET_MODEL_DIR + bin_fn;
    }

    PathLoader(const std::string& basic_path, const std::string& param_fn, const std::string& bin_fn)
            : source(MAKE_UP(PathLoaderSource)())
    {
        source->param_path = basic_path + param_fn;
        source->bin_path = basic_path + bin_fn;
    }

    NcnnNetUP load() override
    {
        NcnnNetUP net = std::make_unique<ncnn::Net>();
        // TODO: check this ...
        net->opt.num_threads = 4;
        net->load_param(source->param_path.c_str());
        net->load_model(source->bin_path.c_str());
        return net;
    }

    PathLoader() = delete;
    PathLoader(const PathLoader&) = delete;
    PathLoader& operator=(const PathLoader&) = delete;

    ~PathLoader() override = default;

private:
    std::unique_ptr<PathLoaderSource> source;
};

} // namespace edet

#endif //EDET_LOADER_PATH_H
