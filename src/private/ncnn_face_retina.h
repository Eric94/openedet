//
// Created by Haotao Lai on 2020-09-15.
//

#ifndef EDET_FACEDETECTORRETINANCNN_H
#define EDET_FACEDETECTORRETINANCNN_H

#include <ncnn/net.h>
#include "detector.h"

namespace edet {

class FaceRetinaNcnn : public DetectorImpl
{
public:
    std::vector<BboxSP> detect(void* rgb, int width, int height) override;
    ~FaceRetinaNcnn() override = default;

    FaceRetinaNcnn();
    explicit FaceRetinaNcnn(ConfigUP config);
    FaceRetinaNcnn(NcnnNetLoaderUP loader, ConfigUP config);

private:
    NcnnNetUP net;
};

} // namespace edet
#endif //EDET_FACEDETECTORRETINANCNN_H
