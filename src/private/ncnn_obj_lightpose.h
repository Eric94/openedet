//
// Created by haotaolai on 2021-01-16.
//

#ifndef EDET_NCNN_OBJ_LIGHTPOSE_H
#define EDET_NCNN_OBJ_LIGHTPOSE_H

#include "detector.h"
#include "loader_path.h"
#include "type.h"

namespace edet {

class ObjUltralightPoseNcnn : public DetectorImpl
{
public:
    std::vector<BboxSP> detect(void* rgb, int width, int height) override;
    ~ObjUltralightPoseNcnn() override = default;

    ObjUltralightPoseNcnn();
    explicit ObjUltralightPoseNcnn(ConfigUP config);
    ObjUltralightPoseNcnn(NcnnNetLoaderUP loader, ConfigUP config);
    ObjUltralightPoseNcnn(int person_label, DetectorImplSP objnet);
    ObjUltralightPoseNcnn(int person_label, DetectorImplSP objnet, NcnnNetLoaderUP loader, ConfigUP config);

private:
    void init();
    void pose_detect(ncnn::Mat& in, const BboxSP& box);

    DetectorImplSP objnet;
    NcnnNetUP posenet;

    const int PERSON_LABEL;
    const float mean_vals[3] = {0.485f * 255.f, 0.456f * 255.f, 0.406f * 255.f};
    const float norm_vals[3] = {1 / 0.229f / 255.f, 1 / 0.224f / 255.f, 1 / 0.225f / 255.f};
};

} // namespace edet
#endif //EDET_NCNN_OBJ_LIGHTPOSE_H
