//
// Created by haotaolai on 2021-01-15.
//

#ifndef EDET_NCNN_OBJ_NANODET_H
#define EDET_NCNN_OBJ_NANODET_H

#include "detector.h"
#include "loader_path.h"

namespace edet {

class ObjNanodetNcnn : public DetectorImpl
{
public:
    std::vector<BboxSP> detect(void* rgb, int width, int height) override;
    ~ObjNanodetNcnn() override = default;

    ObjNanodetNcnn();
    explicit ObjNanodetNcnn(ConfigUP config);
    ObjNanodetNcnn(NcnnNetLoaderUP loader, ConfigUP config);

private:
    NcnnNetUP net;
};

} // namespace edet
#endif //EDET_NCNN_OBJ_NANODET_H
