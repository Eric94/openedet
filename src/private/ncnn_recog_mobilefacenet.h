//
// Created by haotaolai on 2021-01-25.
//

#ifndef EDET_NCNN_RECOG_MOBILEFACENET_H
#define EDET_NCNN_RECOG_MOBILEFACENET_H

#include "recognizer.h"

namespace edet {

class RecogFaceNetMobileNcnn : public RecognizerImpl
{
public:
    EmbeddingSP compute(void* rgb, int width, int height) override;

    RecogFaceNetMobileNcnn();
    explicit RecogFaceNetMobileNcnn(ConfigUP config);
    RecogFaceNetMobileNcnn(NcnnNetLoaderUP loader, ConfigUP config);

private:
    NcnnNetUP net;
};

} // namespace edet
#endif //EDET_NCNN_RECOG_MOBILEFACENET_H
