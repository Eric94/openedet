//
// Created by Haotao Lai on 2020-04-25.
//

#ifndef EDET_BBOX_H
#define EDET_BBOX_H

#include <vector>
#include <memory>
#include "type.h"
#include "platform.h"

namespace edet {

class BboxImpl;
class EDET_EXPORT Bbox
{
public:
    Bbox(float left, float top, float right, float bottom, float score);
    Bbox(float left, float top, float right, float bottom, int id, float score);
    ~Bbox();

    Bbox(const Bbox& b) = delete;
    Bbox& operator=(const Bbox& b) = delete;

    int label() const;
    float x1() const;
    float y1() const;
    float x2() const;
    float y2() const;
    float prob() const;
    float height() const;
    float width() const;
    float area() const;

    bool hasLandmark() const;
    void addLandmark(Point2f point);
    void addLandmark(float x, float y);
    std::vector<Point2f> getLandmarks() const;
    void setLandmark(int i, float x, float y);
    void setLandmark(int i, Point2f point);
    int landmarkSize() const;

    void setX1(float x1);
    void setY1(float y1);
    void setX2(float x2);
    void setY2(float y2);

    bool isSktBox() const;
    void setAsSktBox();
    void addLmpProb(float prob);
    std::vector<float> getLmProbs() const;

    static float inter_area(const std::shared_ptr<Bbox>& b1,
                            const std::shared_ptr<Bbox>& b2);

private:
    std::unique_ptr<BboxImpl> p;
};
} // namespace edet
#endif //EDET_BBOX_H
