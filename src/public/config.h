//
// Created by haotaolai on 2021-01-10.
//

#ifndef EDET_CONFIG_H
#define EDET_CONFIG_H

#include "platform.h"

namespace edet {

namespace {
int get_cpucount()
{
    int count = 0;
#ifdef __EMSCRIPTEN__
    if (emscripten_has_threading_support())
        count = emscripten_num_logical_cores();
    else
        count = 1;
#elif defined __ANDROID__ || defined __linux__
    // get cpu count from /proc/cpuinfo
    FILE* fp = fopen("/proc/cpuinfo", "rb");
    if (!fp)
        return 1;

    char line[1024];
    while (!feof(fp))
    {
        char* s = fgets(line, 1024, fp);
        if (!s)
            break;

        if (memcmp(line, "processor", 9) == 0)
        {
            count++;
        }
    }

    fclose(fp);
#elif __IOS__
    size_t len = sizeof(count);
    sysctlbyname("hw.ncpu", &count, &len, NULL, 0);
#else
#ifdef _OPENMP
    count = omp_get_max_threads();
#else
    count = 1;
#endif // _OPENMP
#endif

    if (count < 1)
        count = 1;

    return count;
}
} // namespace

class EDET_EXPORT Config
{
public:
    Config()
        : comp_img_h(320),
          comp_img_w(320),
          num_of_thread(1),
          min_accepted_prob(0.5f),
          nms_threshold(0.4)
    {
        int cores = get_cpucount();
        if (cores > 1)
            num_of_thread = cores / 2;
    };

public:
    // the actual image size using for computation (by default 320 x 320)
    // the larger size will give better result but take more time
    int comp_img_h;
    int comp_img_w;

    // the threads used for computation openmp
    int num_of_thread;

    // the confident score for accepting the detection as positive result
    float min_accepted_prob;

    // the overlap iou threshold to eliminate duplicate by nms
    float nms_threshold;
};

} // namespace edet

#endif //EDET_CONFIG_H
