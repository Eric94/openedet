//
// Created by haotaolai on 2021-01-28.
//

#ifndef EDET_CONTEXT_H
#define EDET_CONTEXT_H

#include "factory.h"

#include <memory>

namespace edet {

class ContextImpl;
class EDET_EXPORT Context
{
public:
    Context();
    ~Context();

    Context(const Context&) = delete;
    Context& operator=(const Context&) = delete;

    // todo: when we have more than one backend, create different factory here
    std::unique_ptr<Factory>& get_factory();

private:
    static ContextImpl& get_instance();
    ContextImpl& instance;
};


} // namespace edet
#endif //EDET_CONTEXT_H
