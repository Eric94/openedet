//
// Created by Haotao Lai on 2020-09-09.
//

#ifndef EDET_DETECTOR_H
#define EDET_DETECTOR_H

#include <memory>
#include <utility>
#include <ncnn/mat.h>
#include "type.h"
#include "platform.h"

namespace edet {

class DetectorImpl;
typedef std::shared_ptr<DetectorImpl> DetectorImplSP;
typedef std::unique_ptr<DetectorImpl> DetectorImplUP;

// pimpl for the actual detector, will be used by the Factory and the lib user
class EDET_EXPORT Detector
{
public:
    std::vector<BboxSP> detect(void* rgb, int width, int height);
    const ConfigUP& config() const;
    explicit Detector(DetectorImplUP p);
    ~Detector();

    Detector(const Detector&) = delete;
    Detector& operator=(const Detector&) = delete;

private:
    std::unique_ptr<DetectorImpl> p;
};


// actual detector interface, the library user should NOT use it
class DetectorImpl
{
public:
    virtual std::vector<BboxSP> detect(void* rgb, int width, int height) = 0;
    virtual ~DetectorImpl();

    explicit DetectorImpl();
    explicit DetectorImpl(NcnnNetLoaderUP loader);
    DetectorImpl(NcnnNetLoaderUP loader, ConfigUP config);
    const ConfigUP& config() const;

    DetectorImpl(const DetectorImpl&) = delete;
    DetectorImpl& operator=(const DetectorImpl&) = delete;

protected:
    ConfigUP m_config;
    NcnnNetLoaderUP m_loader;
};

} // namespace edet
#endif //EDET_DETECTOR_H
