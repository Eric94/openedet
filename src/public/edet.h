//
// Created by haotaolai on 2021-01-23.
//

#ifndef EDET_EDET_H
#define EDET_EDET_H

#include "context.h"
#include "bbox.h"
#include "config.h"
#include "loader.h"
#include "factory.h"
#include "detector.h"
#include "recognizer.h"
#include "type.h"
#include "version.h"

#endif //EDET_EDET_H
