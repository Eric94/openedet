//
// Created by haotaolai on 2021-01-25.
//

#ifndef EDET_EMBEDDING_H
#define EDET_EMBEDDING_H

#include <memory>
#include "platform.h"
#include "type.h"

namespace edet {

class EmbeddingImpl;
class EDET_EXPORT Embedding
{
public:
    explicit Embedding(int dim);
    ~Embedding();

    int dimension();
    void from_vector(std::vector<float>& source);

    // cosine similarity of two embedding, range [-1, 1]
    static float calc_similarity(const EmbeddingSP& e1, const EmbeddingSP& e2);

private:
    std::unique_ptr<EmbeddingImpl> p;
};
} // namespace edet
#endif //EDET_EMBEDDING_H
