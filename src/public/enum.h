//
// Created by haotaolai on 05/02/21.
//

#ifndef EDET_ENUM_H
#define EDET_ENUM_H

#include "platform.h"

namespace edet {

enum class EDET_EXPORT DetectorEnum {
    UNDEFINED,

    // face detector
    FACE_RETINA_NCNN,

    // object detector
    OBJ_NANODET_NCNN,
    OBJ_LIGHTPOSE_NCNN,
};

enum class EDET_EXPORT RecognizerEnum {
    UNDEFINED,

    FACENET_MOBILE_NCNN,
};

}

#endif //EDET_ENUM_H
