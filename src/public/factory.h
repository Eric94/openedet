//
// Created by haotaolai on 2021-01-23.
//

#ifndef EDET_FACTORY_H
#define EDET_FACTORY_H

#include "platform.h"
#include "enum.h"
#include "type.h"

namespace edet {

class FactoryImpl;
class EDET_EXPORT Factory
{
public:
    Factory();
    ~Factory();

    DetectorUP create_detector(DetectorEnum denum);
    DetectorUP create_detector(DetectorEnum denum, ConfigUP config);

    RecognizerUP create_recognizer(RecognizerEnum renum);
    RecognizerUP create_recognizer(RecognizerEnum renum, ConfigUP config);

private:
    std::unique_ptr<FactoryImpl> p;
};

} // namespace edet
#endif //EDET_FACTORY_H
