//
// Created by haotaolai on 2021-01-10.
//

#ifndef EDET_LOADER_H
#define EDET_LOADER_H

#include <string>
#include <ncnn/net.h>
#include "platform.h"

namespace edet {

class NcnnNetLoader
{
public:
    NcnnNetLoader()
        : EDET_MODEL_DIR(EDET_MODEL_ROOT_PATH)
    {
    }
    virtual ~NcnnNetLoader() = default;
    virtual std::unique_ptr<ncnn::Net> load() = 0;

    std::string EDET_MODEL_DIR;
};

} // namespace edet
#endif //EDET_LOADER_H
