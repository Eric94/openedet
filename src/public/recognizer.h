//
// Created by haotaolai on 2021-01-25.
//

#ifndef EDET_RECOGNIZER_H
#define EDET_RECOGNIZER_H

#include "platform.h"
#include "embedding.h"

namespace edet {

class RecognizerImpl;
typedef std::unique_ptr<RecognizerImpl> RecognizerImplUP;

class EDET_EXPORT Recognizer {
public:
    EmbeddingSP compute(void* rgb, int width, int height);
    const ConfigUP& config() const;

    explicit Recognizer(RecognizerImplUP p);
    ~Recognizer();

    Recognizer(const Recognizer&) = delete;
    Recognizer& operator=(const Recognizer&) = delete;

private:
    std::unique_ptr<RecognizerImpl> p;
};


class RecognizerImpl {
public:
    virtual EmbeddingSP compute(void* rgb, int width, int height) = 0;
    virtual ~RecognizerImpl();

    explicit RecognizerImpl();
    explicit RecognizerImpl(NcnnNetLoaderUP loader);
    RecognizerImpl(NcnnNetLoaderUP loader, ConfigUP config);
    const ConfigUP& config() const;

    RecognizerImpl(const RecognizerImpl&) = delete;
    RecognizerImpl& operator=(const RecognizerImpl&) = delete;

protected:
    ConfigUP m_config;
    NcnnNetLoaderUP m_loader;
};

}

#endif //EDET_RECOGNIZER_H
