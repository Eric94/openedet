//
// Created by haotaolai on 2021-01-22.
//

#ifndef EDET_YPE_HPP
#define EDET_YPE_HPP

#include <memory>
#include "ncnn/net.h"
#include "platform.h"

#define MAKE_UP(CLASS_NAME) std::make_unique<CLASS_NAME>
#define MAKE_SP(CLASS_NAME) std::make_shared<CLASS_NAME>

namespace edet {

class Bbox;
class Detector;
class Config;
class NcnnNetLoader;
class Embedding;
class Recognizer;

typedef std::shared_ptr<Bbox> BboxSP;
typedef std::unique_ptr<Detector> DetectorUP;
typedef std::shared_ptr<Detector> DetectorSP;

typedef std::shared_ptr<Embedding> EmbeddingSP;
typedef std::unique_ptr<Recognizer> RecognizerUP;

typedef std::unique_ptr<NcnnNetLoader> NcnnNetLoaderUP;
typedef std::unique_ptr<Config> ConfigUP;
typedef std::unique_ptr<ncnn::Net> NcnnNetUP;

template<typename Tp>
struct Point_
{
    Point_()
        : x(0), y(0)
    {
    }
    Point_(Tp _x, Tp _y)
        : x(_x), y(_y)
    {
    }

    Tp x;
    Tp y;
};

typedef Point_<float> Point2f;
typedef Point_<int> Point2i;

struct KeyPoint
{
    KeyPoint()
        : p(0, 0), prob(0)
    {

    }
    KeyPoint(float x, float y, float prob)
        : p(x, y), prob(prob)
    {

    }
    KeyPoint(Point2f point, float prob)
        : p(point), prob(prob)
    {

    }

    Point2f p;
    float prob;
};

}

#endif //EDET_YPE_HPP
