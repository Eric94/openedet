//
// Created by haotaolai on 2021-01-22.
//

#ifndef EDET_VERSION_H
#define EDET_VERSION_H

#include "platform.h"

namespace edet {

std::string get_version_str()
{
    return std::to_string(EDET_VERSION_MAJOR) + "."
           + std::to_string(EDET_VERSION_MINOR) + "."
           + std::to_string(EDET_VERSION_PATCH);
}

int get_version_major()
{
    return EDET_VERSION_MAJOR;
}

int get_version_minor()
{
    return EDET_VERSION_MINOR;
}

int get_version_patch()
{
    return EDET_VERSION_PATCH;
}

} // namespace edet

#endif //EDET_VERSION_H
