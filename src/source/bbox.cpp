//
// Created by haotaolai on 2021-01-22.
//

#include "bbox.h"

namespace edet {

class BboxImpl
{
public:
    BboxImpl() = default;
    BboxImpl(float left, float top, float right, float bottom, float score);
    BboxImpl(float left, float top, float right, float bottom, int id, float score);
    ~BboxImpl() = default;

    BboxImpl(const BboxImpl& b) = delete;
    BboxImpl& operator=(const BboxImpl& b) = delete;

    int label() const;
    float x1() const;
    float y1() const;
    float x2() const;
    float y2() const;
    float prob() const;
    float height() const;
    float width() const;
    float area() const;

    bool hasLandmark() const;
    void addLandmark(Point2f point);
    void addLandmark(float x, float y);
    std::vector<Point2f> getLandmarks() const;
    void setLandmark(int i, float x, float y);
    void setLandmark(int i, Point2f point);
    int landmarkSize() const;

    void setX1(float x1);
    void setY1(float y1);
    void setX2(float x2);
    void setY2(float y2);

    bool isSktBox() const;
    void setAsSktBox();
    void addLmpProb(float prob);
    std::vector<float> getLmProbs() const;

    static float inter_area(BboxImpl* b1, BboxImpl* b2);

private:
    // belong to which class,  if it is -1 meaning that the box
    // is produced by a specific com.videri.ai.sdk.detector no need to set id
    int id{};

    // how confident the model believe the detected result
    // belong to this class
    float score{};

    // two corner p to describe a bounding box
    Point2f leftTop, rightBottom;

    // if the com.videri.ai.sdk.detector also produce landmarks
    std::vector<Point2f> landmarks;

    // if the box represent skeleton
    bool isSkt{};
    // prob for each skeleton point (same order as the point stored in landmarks)
    std::vector<float> lmProbs;

    void set(float left, float top, float right, float bottom);
};

BboxImpl::BboxImpl(float left, float top, float right, float bottom, float score)
    : id(-1), score(score), isSkt(false)
{
    set(left, top, right, bottom);
}

BboxImpl::BboxImpl(float left, float top, float right, float bottom, int id, float score)
    : id(id), score(score), isSkt(false)
{
    set(left, top, right, bottom);
}

int BboxImpl::label() const
{
    return id;
}

float BboxImpl::x1() const
{
    return leftTop.x;
}

float BboxImpl::y1() const
{
    return leftTop.y;
}
float BboxImpl::x2() const
{
    return rightBottom.x;
}

float BboxImpl::y2() const
{
    return rightBottom.y;
}

float BboxImpl::prob() const
{
    return score;
}

float BboxImpl::height() const
{
    return rightBottom.y - leftTop.y;
}

float BboxImpl::width() const
{
    return rightBottom.x - leftTop.x;
}

float BboxImpl::area() const
{
    return height() * width();
}

bool BboxImpl::hasLandmark() const
{
    return !landmarks.empty();
}

void BboxImpl::addLandmark(Point2f point)
{
    landmarks.push_back(point);
}

void BboxImpl::addLandmark(float x, float y)
{
    landmarks.emplace_back(x, y);
}

std::vector<edet::Point2f> BboxImpl::getLandmarks() const
{
    return landmarks;
}
void BboxImpl::setLandmark(int i, float x, float y)
{
    Point2f p(x, y);
    landmarks[i] = p;
}

void BboxImpl::setLandmark(int i, Point2f point)
{
    landmarks[i] = point;
}

int BboxImpl::landmarkSize() const
{
    return landmarks.size();
}

void BboxImpl::setX1(float x1)
{
    leftTop.x = x1;
}

void BboxImpl::setY1(float y1)
{
    leftTop.y = y1;
}

void BboxImpl::setX2(float x2)
{
    rightBottom.x = x2;
}

void BboxImpl::setY2(float y2)
{
    rightBottom.y = y2;
}

bool BboxImpl::isSktBox() const
{
    return isSkt;
}

void BboxImpl::setAsSktBox()
{
    isSkt = true;
}

void BboxImpl::addLmpProb(float prob)
{
    lmProbs.push_back(prob);
}

std::vector<float> BboxImpl::getLmProbs() const
{
    return lmProbs;
}

float BboxImpl::inter_area(BboxImpl* b1, BboxImpl* b2)
{
    float x1 = std::max<>(b1->x1(), b2->x1());
    float y1 = std::max<>(b1->y1(), b2->y1());
    float x2 = std::min<>(b1->x2(), b2->x2());
    float y2 = std::min<>(b1->y2(), b2->y2());

    if (x2 < x1 || y2 < y1) return 0;
    return (x2 - x1) * (y2 - y1);
}

void BboxImpl::set(float left, float top, float right, float bottom)
{
    leftTop.x = left;
    leftTop.y = top;
    rightBottom.x = right;
    rightBottom.y = bottom;
}

// ******************************************************************************************* //
Bbox::Bbox(float left, float top, float right, float bottom, float score)
    :p(std::make_unique<BboxImpl>(left, top, right, bottom, score))
{
}
Bbox::Bbox(float left, float top, float right, float bottom, int id, float score)
    :p(std::make_unique<BboxImpl>(left, top, right, bottom, id, score))
{
}

Bbox::~Bbox() = default;

int Bbox::label() const
{
    return p->label();
}
float Bbox::x1() const
{
    return p->x1();
}
float Bbox::y1() const
{
    return p->y1();
}
float Bbox::x2() const
{
    return p->x2();
}
float Bbox::y2() const
{
    return p->y2();
}
float Bbox::prob() const
{
    return p->prob();
}
float Bbox::height() const
{
    return p->height();
}
float Bbox::width() const
{
    return p->width();
}
float Bbox::area() const
{
    return p->area();
}
bool Bbox::hasLandmark() const
{
    return p->hasLandmark();
}
void Bbox::addLandmark(Point2f point)
{
    p->addLandmark(point);
}
void Bbox::addLandmark(float x, float y)
{
    p->addLandmark(x, y);
}
std::vector<Point2f> Bbox::getLandmarks() const
{
    return p->getLandmarks();
}
void Bbox::setLandmark(int i, float x, float y)
{
    p->setLandmark(i, x, y);
}
void Bbox::setLandmark(int i, Point2f point)
{
    p->setLandmark(i, point);
}
int Bbox::landmarkSize() const
{
    return p->landmarkSize();
}
void Bbox::setX1(float x1)
{
    p->setX1(x1);
}
void Bbox::setY1(float y1)
{
    p->setY1(y1);
}
void Bbox::setX2(float x2)
{
    p->setX2(x2);
}
void Bbox::setY2(float y2)
{
    p->setY2(y2);
}
bool Bbox::isSktBox() const
{
    return p->isSktBox();
}
void Bbox::setAsSktBox()
{
    p->setAsSktBox();
}
void Bbox::addLmpProb(float prob)
{
    p->addLmpProb(prob);
}
std::vector<float> Bbox::getLmProbs() const
{
    return p->getLmProbs();
}
float Bbox::inter_area(const std::shared_ptr<Bbox>& b1, const std::shared_ptr<Bbox>& b2)
{
    return BboxImpl::inter_area(b1->p.get(), b2->p.get());
}

} // namespace edet