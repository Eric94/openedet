//
// Created by haotaolai on 2021-01-28.
//

#include "context.h"
#include "type.h"

namespace edet {

// singleton class
class ContextImpl
{
public:
    ContextImpl();
    ~ContextImpl() = default;

    ContextImpl(const ContextImpl&) = delete;
    ContextImpl& operator=(const ContextImpl&) = delete;

    std::unique_ptr<Factory> factory;
};

ContextImpl::ContextImpl()
    : factory(MAKE_UP(Factory)())
{
}

Context::Context()
    : instance(get_instance())
{
}

Context::~Context() = default;

ContextImpl& Context::get_instance()
{
    static ContextImpl instance_;
    return instance_;
}
std::unique_ptr<Factory>& Context::get_factory()
{
    return instance.factory;
}

} // namespace edet