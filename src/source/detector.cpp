//
// Created by haotaolai on 2021-01-22.
//
#include "detector.h"
#include "config.h"
#include "loader.h"
#include "bbox.h"

using namespace edet;

DetectorImpl::DetectorImpl()
    : m_loader(nullptr), m_config(MAKE_UP(Config)())
{
}

DetectorImpl::DetectorImpl(NcnnNetLoaderUP loader)
    : m_loader(std::move(loader)), m_config(MAKE_UP(Config)())
{
}

DetectorImpl::DetectorImpl(NcnnNetLoaderUP loader, ConfigUP config)
    : m_loader(std::move(loader)), m_config(std::move(config))
{
}

DetectorImpl::~DetectorImpl() = default;

const ConfigUP& DetectorImpl::config() const
{
    return m_config;
}


std::vector<BboxSP> Detector::detect(void* rgb, int width, int height)
{
    return p->detect(rgb, width, height);
}

Detector::Detector(DetectorImplUP p)
    : p(std::move(p))
{
}
const ConfigUP& Detector::config() const
{
    return p->config();
}

Detector::~Detector() = default;
