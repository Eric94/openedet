//
// Created by haotaolai on 2021-01-10.
//
#include <cmath>
#include "dethelper.h"
#include "bbox.h"

using namespace edet;

void Helper::qsort_descent_inplace(std::vector<BboxSP>& bbs, int left, int right)
{
    int i = left;
    int j = right;
    float p = bbs[(left + right) / 2]->prob();

    while (i <= j)
    {
        while (bbs[i]->prob() > p) i++;
        while (bbs[j]->prob() < p) j--;

        if (i <= j)
        {
            // swap
            std::swap(bbs[i], bbs[j]);

            i++;
            j--;
        }
    }

#pragma omp parallel sections
    {
#pragma omp section
        {
            if (left < j) qsort_descent_inplace(bbs, left, j);
        }
#pragma omp section
        {
            if (i < right) qsort_descent_inplace(bbs, i, right);
        }
    }
}

void Helper::qsort_descent_inplace(std::vector<BboxSP>& bbs)
{
    if (bbs.empty()) return;
    qsort_descent_inplace(bbs, 0, bbs.size() - 1);
}

void Helper::nms_sorted_bboxes(const std::vector<BboxSP>& bbs,
                       std::vector<int>& picked,
                       float nms_threshold)
{
    picked.clear();

    const int n = bbs.size();

    std::vector<float> areas(n);
    for (int i = 0; i < n; i++)
    {
        areas[i] = bbs[i]->area();
    }

    for (int i = 0; i < n; i++)
    {
        const BboxSP& a = bbs[i];

        bool keep = true;
        for (int j = 0; j < (int)picked.size(); j++)
        {
            const BboxSP& b = bbs[picked[j]];

            // intersection over union
            float inter_area = Bbox::inter_area(a, b);
            float union_area = areas[i] + areas[picked[j]] - inter_area;
            float iou = inter_area / union_area;

            if (iou > nms_threshold) keep = false;
        }

        if (keep) picked.push_back(i);
    }
}

// copy from src/layer/proposal.cpp
ncnn::Mat Helper::generate_anchors(int base_size,
                           const ncnn::Mat& ratios,
                           const ncnn::Mat& scales)
{
    int num_ratio = ratios.w;
    int num_scale = scales.w;

    ncnn::Mat anchors;
    anchors.create(4, num_ratio * num_scale);

    const float cx = base_size * 0.5f;
    const float cy = base_size * 0.5f;

    for (int i = 0; i < num_ratio; i++)
    {
        float ar = ratios[i];

        int r_w = round(base_size / sqrt(ar));
        int r_h = round(r_w * ar); //round(base_size * sqrt(ar));

        for (int j = 0; j < num_scale; j++)
        {
            float scale = scales[j];

            float rs_w = r_w * scale;
            float rs_h = r_h * scale;

            float* anchor = anchors.row(i * num_scale + j);

            anchor[0] = cx - rs_w * 0.5f;
            anchor[1] = cy - rs_h * 0.5f;
            anchor[2] = cx + rs_w * 0.5f;
            anchor[3] = cy + rs_h * 0.5f;
        }
    }

    return anchors;
}
