//
// Created by haotaolai on 2021-01-25.
//

#include <vector>
#include <cassert>
#include "embedding.h"

static void normalize(std::vector<float>& vector)
{
    float sum = 0;
    for (const float& k : vector)
        sum += k * k;
    float len = sqrt(sum);

    for (float& k : vector)
        k /= len;
}

namespace edet {

typedef std::unique_ptr<EmbeddingImpl> EmbeddingImplUp;

class EmbeddingImpl
{
public:
    explicit EmbeddingImpl(int dim);
    ~EmbeddingImpl();

    int dimension();
    void from_vector(std::vector<float>& source);
    static float calc_similarity(const EmbeddingImplUp& e1, const EmbeddingImplUp& e2);

private:
    std::vector<float> embedding;
};

EmbeddingImpl::EmbeddingImpl(int dim)
    : embedding(std::vector<float>(dim))
{
}
float EmbeddingImpl::calc_similarity(const EmbeddingImplUp& e1, const EmbeddingImplUp& e2)
{
    assert(e1->dimension() == e2->dimension());
    float sim = 0.0;

    // http://mlwiki.org/index.php/Cosine_Similarity
    // when the embedding is saved via from_vector function, it has been normalized
    // for unit vector, the cosine similarity will be just the dot product of them
    for (int i = 0; i < e1->embedding.size(); i++)
        sim += e1->embedding[i] * e2->embedding[i];

    return sim;
}
int EmbeddingImpl::dimension()
{
    return embedding.size();
}
void EmbeddingImpl::from_vector(std::vector<float>& source)
{
    assert(source.size() == embedding.size());
    normalize(source);
    embedding.assign(source.begin(), source.end());
}
EmbeddingImpl::~EmbeddingImpl() = default;

// *********************************************************************** //
Embedding::Embedding(int dim)
    : p(MAKE_UP(EmbeddingImpl)(dim))
{
}
float Embedding::calc_similarity(const EmbeddingSP& e1, const EmbeddingSP& e2)
{
    return EmbeddingImpl::calc_similarity(e1->p, e2->p);
}
int Embedding::dimension()
{
    return p->dimension();
}
void Embedding::from_vector(std::vector<float>& source)
{
    p->from_vector(source);
}
Embedding::~Embedding() = default;

} // namespace edet