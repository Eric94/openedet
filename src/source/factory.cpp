//
// Created by haotaolai on 2021-01-23.
//
#include "factory.h"
#include "ncnn_face_retina.h"
#include "ncnn_obj_nanodet.h"
#include "ncnn_obj_lightpose.h"
#include "ncnn_recog_mobilefacenet.h"
#include "config.h"

static void log_config(const std::unique_ptr<edet::Config>& config)
{
    EDET_LOGI("************* Config Info *************");
    EDET_LOGI("> number of thread: %d", config->num_of_thread);
    EDET_LOGI("> input size(w, h): [%d, %d]", config->comp_img_w, config->comp_img_h);
    EDET_LOGI("***************************************");
    EDET_LOGI("");
}

namespace edet {

class FactoryImpl
{
public:
    DetectorUP create_detector(DetectorEnum denum);
    DetectorUP create_detector(DetectorEnum denum, ConfigUP config);
    RecognizerUP create_recognizer(RecognizerEnum renum);
    RecognizerUP create_recognizer(RecognizerEnum renum, ConfigUP config);
};

DetectorUP FactoryImpl::create_detector(DetectorEnum denum)
{
    // set config to nullptr for default configuration setting
    return create_detector(denum, nullptr);
}

DetectorUP FactoryImpl::create_detector(DetectorEnum denum, ConfigUP config)
{
    DetectorUP p;
    switch (denum)
    {
    case DetectorEnum::FACE_RETINA_NCNN:
        if (config == nullptr)
            p = MAKE_UP(Detector)(MAKE_UP(FaceRetinaNcnn)());
        else
            p = MAKE_UP(Detector)(MAKE_UP(FaceRetinaNcnn)(std::move(config)));
        break;
    case DetectorEnum::OBJ_NANODET_NCNN:
        if (config == nullptr)
            p = MAKE_UP(Detector)(MAKE_UP(ObjNanodetNcnn)());
        else
            p = MAKE_UP(Detector)(MAKE_UP(ObjNanodetNcnn)(std::move(config)));
        break;
    case DetectorEnum::OBJ_LIGHTPOSE_NCNN:
        if (config == nullptr)
            p = MAKE_UP(Detector)(MAKE_UP(ObjUltralightPoseNcnn)());
        else
            p = MAKE_UP(Detector)(MAKE_UP(ObjUltralightPoseNcnn)(std::move(config)));
        break;
    }
    log_config(p->config());
    return p;
}

RecognizerUP FactoryImpl::create_recognizer(RecognizerEnum renum)
{
    // set config to nullptr for default configuration setting
    return create_recognizer(renum, nullptr);
}

RecognizerUP FactoryImpl::create_recognizer(RecognizerEnum renum, ConfigUP config)
{
    RecognizerUP p;
    switch (renum)
    {
    case RecognizerEnum::FACENET_MOBILE_NCNN:
        if (config == nullptr)
            p = MAKE_UP(Recognizer)(MAKE_UP(RecogFaceNetMobileNcnn)());
        else
            p = MAKE_UP(Recognizer)(MAKE_UP(RecogFaceNetMobileNcnn)(std::move(config)));
        break;
    }
    log_config(p->config());
    return p;
}

DetectorUP Factory::create_detector(DetectorEnum denum)
{
    return p->create_detector(denum);
}
DetectorUP Factory::create_detector(DetectorEnum denum, ConfigUP config)
{
    return p->create_detector(denum, std::move(config));
}
RecognizerUP Factory::create_recognizer(RecognizerEnum renum)
{
    return p->create_recognizer(renum);
}
RecognizerUP Factory::create_recognizer(RecognizerEnum renum, ConfigUP config)
{
    return p->create_recognizer(renum, std::move(config));
}
Factory::Factory()
    : p(MAKE_UP(FactoryImpl)())
{
}
Factory::~Factory() = default;
} // namespace edet
