#include "ncnn_face_retina.h"
#include <ncnn/mat.h>
#include "bbox.h"
#include "config.h"
#include "dethelper.h"
#include "loader_path.h"
#include "config_const.h"

using namespace edet;

static void generate_proposals(const ncnn::Mat& anchors,
                               int feat_stride,
                               const ncnn::Mat& score_blob,
                               const ncnn::Mat& bbox_blob,
                               const ncnn::Mat& landmark_blob,
                               float prob_threshold,
                               std::vector<BboxSP>& faceobjects)
{
    int w = score_blob.w;
    int h = score_blob.h;

    // generate face proposal from bbox deltas and shifted anchors
    const int num_anchors = anchors.h;

    for (int q = 0; q < num_anchors; q++)
    {
        const float* anchor = anchors.row(q);

        const ncnn::Mat score = score_blob.channel(q + num_anchors);
        const ncnn::Mat bbox = bbox_blob.channel_range(q * 4, 4);
        const ncnn::Mat landmark = landmark_blob.channel_range(q * 10, 10);

        // shifted anchor
        float anchor_y = anchor[1];

        float anchor_w = anchor[2] - anchor[0];
        float anchor_h = anchor[3] - anchor[1];

        for (int i = 0; i < h; i++)
        {
            float anchor_x = anchor[0];

            for (int j = 0; j < w; j++)
            {
                int index = i * w + j;

                float prob = score[index];

                if (prob >= prob_threshold)
                {
                    // apply center size
                    float dx = bbox.channel(0)[index];
                    float dy = bbox.channel(1)[index];
                    float dw = bbox.channel(2)[index];
                    float dh = bbox.channel(3)[index];

                    float cx = anchor_x + anchor_w * 0.5f;
                    float cy = anchor_y + anchor_h * 0.5f;

                    float pb_cx = cx + anchor_w * dx;
                    float pb_cy = cy + anchor_h * dy;

                    float pb_w = anchor_w * exp(dw);
                    float pb_h = anchor_h * exp(dh);

                    float x0 = pb_cx - pb_w * 0.5f;
                    float y0 = pb_cy - pb_h * 0.5f;
                    float x1 = pb_cx + pb_w * 0.5f;
                    float y1 = pb_cy + pb_h * 0.5f;

                    int k = 0;
                    BboxSP box = std::make_shared<Bbox>(x0, y0, x1, y1, prob);
                    while (k < 10)
                    {
                        float p1 = cx + (anchor_w + 1) * landmark.channel(k++)[index];
                        float p2 = cy + (anchor_h + 1) * landmark.channel(k++)[index];
                        box->addLandmark(p1, p2);
                    }
                    faceobjects.push_back(box);
                }

                anchor_x += feat_stride;
            }

            anchor_y += feat_stride;
        }
    }
}

std::vector<BboxSP> FaceRetinaNcnn::detect(void* rgb, int width, int height)
{
    auto image = ncnn::Mat::from_pixels_resize(
        (unsigned char*)rgb, ncnn::Mat::PIXEL_RGB,
        width, height, m_config->comp_img_w, m_config->comp_img_h);

    ncnn::Extractor ex = net->create_extractor();
    //ex.set_num_threads(m_config->num_of_thread);

    ex.input("data", image);

    std::vector<BboxSP> faceproposals;

    // stride 32
    {
        ncnn::Mat score_blob, bbox_blob, landmark_blob;
        ex.extract("face_rpn_cls_prob_reshape_stride32", score_blob);
        ex.extract("face_rpn_bbox_pred_stride32", bbox_blob);
        ex.extract("face_rpn_landmark_pred_stride32", landmark_blob);

        const int base_size = 16;
        const int feat_stride = 32;
        ncnn::Mat ratios(1);
        ratios[0] = 1.f;
        ncnn::Mat scales(2);
        scales[0] = 32.f;
        scales[1] = 16.f;
        ncnn::Mat anchors = Helper::generate_anchors(base_size, ratios, scales);

        std::vector<BboxSP> faceobjects32;
        generate_proposals(anchors, feat_stride, score_blob, bbox_blob, landmark_blob,
                           m_config->min_accepted_prob, faceobjects32);

        faceproposals.insert(faceproposals.end(), faceobjects32.begin(), faceobjects32.end());
    }

    // stride 16
    {
        ncnn::Mat score_blob, bbox_blob, landmark_blob;
        ex.extract("face_rpn_cls_prob_reshape_stride16", score_blob);
        ex.extract("face_rpn_bbox_pred_stride16", bbox_blob);
        ex.extract("face_rpn_landmark_pred_stride16", landmark_blob);

        const int base_size = 16;
        const int feat_stride = 16;
        ncnn::Mat ratios(1);
        ratios[0] = 1.f;
        ncnn::Mat scales(2);
        scales[0] = 8.f;
        scales[1] = 4.f;
        ncnn::Mat anchors = Helper::generate_anchors(base_size, ratios, scales);

        std::vector<BboxSP> faceobjects16;
        generate_proposals(anchors, feat_stride, score_blob, bbox_blob, landmark_blob,
                           m_config->min_accepted_prob, faceobjects16);

        faceproposals.insert(faceproposals.end(), faceobjects16.begin(), faceobjects16.end());
    }

    // stride 8
    {
        ncnn::Mat score_blob, bbox_blob, landmark_blob;
        ex.extract("face_rpn_cls_prob_reshape_stride8", score_blob);
        ex.extract("face_rpn_bbox_pred_stride8", bbox_blob);
        ex.extract("face_rpn_landmark_pred_stride8", landmark_blob);

        const int base_size = 16;
        const int feat_stride = 8;
        ncnn::Mat ratios(1);
        ratios[0] = 1.f;
        ncnn::Mat scales(2);
        scales[0] = 2.f;
        scales[1] = 1.f;
        ncnn::Mat anchors = Helper::generate_anchors(base_size, ratios, scales);

        std::vector<BboxSP> faceobjects8;
        generate_proposals(anchors, feat_stride, score_blob, bbox_blob, landmark_blob,
                           m_config->min_accepted_prob, faceobjects8);

        faceproposals.insert(faceproposals.end(), faceobjects8.begin(), faceobjects8.end());
    }

    // sort all proposals by score from highest to lowest
    Helper::qsort_descent_inplace(faceproposals);

    // apply nms with nms_threshold
    std::vector<int> picked;
    Helper::nms_sorted_bboxes(faceproposals, picked, m_config->nms_threshold);

    int face_count = picked.size();
    std::vector<BboxSP> res;

    for (int i = 0; i < face_count; i++)
    {
        auto& tmpbox = faceproposals[picked[i]];

        float x1 = tmpbox->x1() / m_config->comp_img_w * width;
        float y1 = tmpbox->y1() / m_config->comp_img_h * height;
        float x2 = tmpbox->x2() / m_config->comp_img_w * width;
        float y2 = tmpbox->y2() / m_config->comp_img_h * height;

        x1 = std::max<>(std::min<>(x1, (float)width - 1), 0.f);
        y1 = std::max<>(std::min<>(y1, (float)height - 1), 0.f);
        x2 = std::max<>(std::min<>(x2, (float)width - 1), 0.f);
        y2 = std::max<>(std::min<>(y2, (float)height - 1), 0.f);

        auto box = std::make_shared<Bbox>(x1, y1, x2, y2, tmpbox->prob());
        for (auto& k : tmpbox->getLandmarks())
        {
            box->addLandmark(k.x / m_config->comp_img_w * width,
                             k.y / m_config->comp_img_h * height);
        }
        res.push_back(box);
    }
    return res;
}

FaceRetinaNcnn::FaceRetinaNcnn()
    : DetectorImpl(MAKE_UP(PathLoader)(config_const::retinface_fn + ".param",
                                       config_const::retinface_fn + ".bin"))
{
    m_config->comp_img_h = 500;
    m_config->comp_img_w = 500;
    m_config->min_accepted_prob = 0.8f;
    m_config->nms_threshold = 0.4f;

    net = m_loader->load();
}

FaceRetinaNcnn::FaceRetinaNcnn(ConfigUP config)
    : DetectorImpl(MAKE_UP(PathLoader)(config_const::retinface_fn + ".param",
                                       config_const::retinface_fn + ".bin"),
                   std::move(config))
{
    net = m_loader->load();
}

FaceRetinaNcnn::FaceRetinaNcnn(NcnnNetLoaderUP loader, ConfigUP config)
    : DetectorImpl(std::move(loader), std::move(config))
{
    net = m_loader->load();
}
