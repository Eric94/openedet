//
// Created by haotaolai on 2021-01-16.
//

#include "ncnn_obj_lightpose.h"
#include <cstring>
#include "config.h"
#include "bbox.h"
#include "ncnn_obj_nanodet.h"
#include "config_const.h"

using namespace edet;

void ObjUltralightPoseNcnn::pose_detect(ncnn::Mat& in, const BboxSP& box)
{
    in.substract_mean_normalize(mean_vals, norm_vals);

    ncnn::Extractor ex = posenet->create_extractor();
    //ex.set_num_threads(m_config->num_of_thread);

    ex.input("data", in);
    ncnn::Mat out;

    ex.extract("hybridsequential0_conv7_fwd", out);

    // resolve p from heatmap
    for (int p = 0; p < out.c; p++)
    {
        const ncnn::Mat m = out.channel(p);

        float max_prob = 0.f;
        int max_x = 0;
        int max_y = 0;
        for (int y = 0; y < out.h; y++)
        {
            const float* ptr = m.row(y);
            for (int x = 0; x < out.w; x++)
            {
                float prob = ptr[x];
                if (prob > max_prob)
                {
                    max_prob = prob;
                    max_x = x;
                    max_y = y;
                }
            }
        }

        box->addLandmark(max_x * box->width() / (float) out.w + box->x1(), max_y * box->height() / (float) out.h + box->y1());
        box->addLmpProb(max_prob);
    }
}


std::vector<BboxSP> ObjUltralightPoseNcnn::detect(void* rgb, int width, int height)
{
    std::vector<BboxSP> res;

    int len = width * height * 3;
    void* copy = std::malloc(len);
    std::memcpy(copy, rgb, len);

    const std::vector<BboxSP>& bbs = objnet->detect(rgb, width, height);
    for (auto& box : bbs)
    {
        if (box->label() != PERSON_LABEL) continue;

        void* copy2 = std::malloc(len);
        std::memcpy(copy2, copy, len);

        ncnn::Mat roi = ncnn::Mat::from_pixels_roi_resize(
            (unsigned char*)copy2, ncnn::Mat::PIXEL_RGB, width, height, box->x1(), box->y1(),
            box->width(), box->height(), m_config->comp_img_w, m_config->comp_img_h);

        pose_detect(roi, box);
        box->setAsSktBox();

        res.push_back(box);
        std::free(copy2);
    }

    std::free(copy);
    return res;
}

void ObjUltralightPoseNcnn::init()
{
    // m_config is for pose model only, person model comes with its own m_config
    m_config->comp_img_w = 192;
    m_config->comp_img_h = 256;

    // wont' use these two in pose model
    m_config->min_accepted_prob = 0;
    m_config->nms_threshold = 0;
}

ObjUltralightPoseNcnn::ObjUltralightPoseNcnn()
    : DetectorImpl(MAKE_UP(PathLoader)("ultralight-pose.param", "ultralight-pose.bin")),
      objnet(MAKE_SP(ObjNanodetNcnn)()),
      PERSON_LABEL(0)
{
    init();
    posenet = m_loader->load();
}

ObjUltralightPoseNcnn::ObjUltralightPoseNcnn(edet::ConfigUP config)
    : DetectorImpl(MAKE_UP(PathLoader)(config_const::ultralightpose_fn + ".param",
                                       config_const::ultralightpose_fn + ".bin"),
                   std::move(config)),
      objnet(MAKE_SP(ObjNanodetNcnn)()),
      PERSON_LABEL(0)
{
    init(); // force the input image w and h to the preset value
    posenet = m_loader->load();
}
ObjUltralightPoseNcnn::ObjUltralightPoseNcnn(NcnnNetLoaderUP loader, edet::ConfigUP config)
    : DetectorImpl(std::move(loader), std::move(config)),
      objnet(MAKE_SP(ObjNanodetNcnn)()),
      PERSON_LABEL(0)
{
    init(); // force the input image w and h to the preset value
    posenet = m_loader->load();
}

ObjUltralightPoseNcnn::ObjUltralightPoseNcnn(int person_label, DetectorImplSP objnet)
    : DetectorImpl(MAKE_UP(PathLoader)(config_const::ultralightpose_fn + ".param",
                                       config_const::ultralightpose_fn + ".bin")),
      objnet(objnet),
      PERSON_LABEL(person_label)
{
    init();
    posenet = m_loader->load();
}

ObjUltralightPoseNcnn::ObjUltralightPoseNcnn(int person_label, DetectorImplSP objnet,
                                             NcnnNetLoaderUP loader, ConfigUP config)
    : DetectorImpl(std::move(loader), std::move(config)),
      objnet(objnet),
      PERSON_LABEL(person_label)
{
    init(); // force the input image w and h to the preset value
    posenet = m_loader->load();
}

