//
// Created by haotaolai on 2021-01-15.
//

#include "ncnn_obj_nanodet.h"
#include <ncnn/mat.h>
#include <cfloat>
#include "dethelper.h"
#include "bbox.h"
#include "config.h"
#include "config_const.h"

using namespace edet;

static void generate_proposals(
    const ncnn::Mat& cls_pred,
    const ncnn::Mat& dis_pred,
    int stride,
    const ncnn::Mat& in_pad,
    float prob_threshold,
    std::vector<BboxSP>& objects)
{
    const int num_grid = cls_pred.h;

    int num_grid_x;
    int num_grid_y;
    if (in_pad.w > in_pad.h)
    {
        num_grid_x = in_pad.w / stride;
        num_grid_y = num_grid / num_grid_x;
    }
    else
    {
        num_grid_y = in_pad.h / stride;
        num_grid_x = num_grid / num_grid_y;
    }

    const int num_class = cls_pred.w;
    const int reg_max_1 = dis_pred.w / 4;

    for (int i = 0; i < num_grid_y; i++)
    {
        for (int j = 0; j < num_grid_x; j++)
        {
            const int idx = i * num_grid_x + j;

            const float* scores = cls_pred.row(idx);

            // find label with max score
            int label = -1;
            float score = -FLT_MAX;
            for (int k = 0; k < num_class; k++)
            {
                if (scores[k] > score)
                {
                    label = k;
                    score = scores[k];
                }
            }

            if (score >= prob_threshold)
            {
                ncnn::Mat bbox_pred(reg_max_1, 4, (void*)dis_pred.row(idx));
                {
                    ncnn::Layer* softmax = ncnn::create_layer("Softmax");

                    ncnn::ParamDict pd;
                    pd.set(0, 1); // axis
                    pd.set(1, 1);
                    softmax->load_param(pd);

                    ncnn::Option opt;
                    opt.num_threads = 1;
                    opt.use_packing_layout = false;

                    softmax->create_pipeline(opt);

                    softmax->forward_inplace(bbox_pred, opt);

                    softmax->destroy_pipeline(opt);

                    delete softmax;
                }

                float dis_pred[4];
                for (int k = 0; k < 4; k++)
                {
                    float dis = 0.f;
                    const float* dis_after_sm = bbox_pred.row(k);
                    for (int l = 0; l < reg_max_1; l++)
                    {
                        dis += l * dis_after_sm[l];
                    }

                    dis_pred[k] = dis * stride;
                }

                float pb_cx = (j + 0.5f) * stride;
                float pb_cy = (i + 0.5f) * stride;

                float x0 = pb_cx - dis_pred[0];
                float y0 = pb_cy - dis_pred[1];
                float x1 = pb_cx + dis_pred[2];
                float y1 = pb_cy + dis_pred[3];

                BboxSP box = std::make_shared<Bbox>(x0, y0, x1, y1, label, score);
                objects.push_back(box);
            }
        }
    }
}

std::vector<BboxSP> ObjNanodetNcnn::detect(void* rgb, int width, int height)
{
    // pad to multiple of 32
    int w = width;
    int h = height;
    float scale = 1.f;
    if (w > h)
    {
        scale = (float)m_config->comp_img_w / w;
        w = m_config->comp_img_w;
        h = h * scale;
    }
    else
    {
        scale = (float)m_config->comp_img_h / h;
        h = m_config->comp_img_w;
        w = w * scale;
    }

    ncnn::Mat in = ncnn::Mat::from_pixels_resize(
        (unsigned char*)rgb, ncnn::Mat::PIXEL_RGB2BGR, width, height, w, h);

    // pad to target_size rectangle
    int wpad = (w + 31) / 32 * 32 - w;
    int hpad = (h + 31) / 32 * 32 - h;
    ncnn::Mat in_pad;
    ncnn::copy_make_border(in, in_pad, hpad / 2, hpad - hpad / 2, wpad / 2, wpad - wpad / 2, ncnn::BORDER_CONSTANT, 0.f);

    const float mean_vals[3] = {103.53f, 116.28f, 123.675f};
    const float norm_vals[3] = {0.017429f, 0.017507f, 0.017125f};
    in_pad.substract_mean_normalize(mean_vals, norm_vals);

    ncnn::Extractor ex = net->create_extractor();

    ex.input("input.1", in_pad);

    std::vector<BboxSP> proposals;

    // stride 8
    {
        ncnn::Mat cls_pred;
        ncnn::Mat dis_pred;
        ex.extract("792", cls_pred);
        ex.extract("795", dis_pred);

        std::vector<BboxSP> objects8;
        generate_proposals(cls_pred, dis_pred, 8, in_pad, m_config->min_accepted_prob, objects8);

        proposals.insert(proposals.end(), objects8.begin(), objects8.end());
    }

    // stride 16
    {
        ncnn::Mat cls_pred;
        ncnn::Mat dis_pred;
        ex.extract("814", cls_pred);
        ex.extract("817", dis_pred);

        std::vector<BboxSP> objects16;
        generate_proposals(cls_pred, dis_pred, 16, in_pad, m_config->min_accepted_prob, objects16);

        proposals.insert(proposals.end(), objects16.begin(), objects16.end());
    }

    // stride 32
    {
        ncnn::Mat cls_pred;
        ncnn::Mat dis_pred;
        ex.extract("836", cls_pred);
        ex.extract("839", dis_pred);

        std::vector<BboxSP> objects32;
        generate_proposals(cls_pred, dis_pred, 32, in_pad, m_config->min_accepted_prob, objects32);

        proposals.insert(proposals.end(), objects32.begin(), objects32.end());
    }

    // sort all proposals by score from highest to lowest
    Helper::qsort_descent_inplace(proposals);

    // apply nms with nms_threshold
    std::vector<int> picked;
    Helper::nms_sorted_bboxes(proposals, picked, m_config->nms_threshold);

    int count = picked.size();
    std::vector<BboxSP> bbs;

    for (int i = 0; i < count; i++)
    {
        auto& tmpbox = proposals[picked[i]];

        // adjust offset to original unpadded
        float x0 = (tmpbox->x1() - (wpad / 2)) / scale;
        float y0 = (tmpbox->y1()- (hpad / 2)) / scale;
        float x1 = (tmpbox->x2() - (wpad / 2)) / scale;
        float y1 = (tmpbox->y2() - (hpad / 2)) / scale;

        // clip
        x0 = std::max<>(std::min<>(x0, (float)(width - 1)), 0.f);
        y0 = std::max<>(std::min<>(y0, (float)(height - 1)), 0.f);
        x1 = std::max<>(std::min<>(x1, (float)(width - 1)), 0.f);
        y1 = std::max<>(std::min<>(y1, (float)(height - 1)), 0.f);

        // add into the result list
        bbs.push_back(std::make_shared<Bbox>(x0, y0, x1, y1, tmpbox->label(), tmpbox->prob()));
    }

    return bbs;
}

ObjNanodetNcnn::ObjNanodetNcnn()
    : DetectorImpl(MAKE_UP(PathLoader)(config_const::nanodet_fn + ".param",
                                       config_const::nanodet_fn + ".bin"))
{
    m_config->comp_img_w = 320;
    m_config->comp_img_h = 320;
    m_config->min_accepted_prob = 0.4f;
    m_config->nms_threshold = 0.5f;

    net = m_loader->load();
}

ObjNanodetNcnn::ObjNanodetNcnn(ConfigUP config)
    : DetectorImpl(MAKE_UP(PathLoader)(config_const::nanodet_fn + ".param",
                                       config_const::nanodet_fn + ".bin"),
                   std::move(config))
{
    net = m_loader->load();
}

ObjNanodetNcnn::ObjNanodetNcnn(NcnnNetLoaderUP loader, ConfigUP config)
    : DetectorImpl(std::move(loader), std::move(config))
{
    net = m_loader->load();
}
