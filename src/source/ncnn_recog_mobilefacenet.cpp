//
// Created by haotaolai on 2021-01-25.
//

#include "ncnn_recog_mobilefacenet.h"
#include "loader_path.h"
#include "config.h"
#include "config_const.h"

using namespace edet;

EmbeddingSP RecogFaceNetMobileNcnn::compute(void* rgb, int width, int height)
{
    auto image = ncnn::Mat::from_pixels_resize(
        (unsigned char*)rgb, ncnn::Mat::PIXEL_RGB,
        width, height, m_config->comp_img_w, m_config->comp_img_h);

    ncnn::Extractor ex = net->create_extractor();
    ex.input("data", image);
    ncnn::Mat out;
    ex.extract("fc1", out);

    std::vector<float> out_vector(128);
    for (int j = 0; j < 128; j++)
        out_vector[j] = out[j];

    std::shared_ptr<Embedding> embedding = std::make_shared<Embedding>(128);
    embedding->from_vector(out_vector);
    return embedding;
}

RecogFaceNetMobileNcnn::RecogFaceNetMobileNcnn()
    : RecognizerImpl(MAKE_UP(PathLoader)(config_const::mobilefacenet + ".param",
                                         config_const::mobilefacenet + ".bin"))
{
    m_config->comp_img_h = 112;
    m_config->comp_img_w = 112;

    // wont' use these two in current model
    m_config->min_accepted_prob = 0;
    m_config->nms_threshold = 0;

    net = m_loader->load();
}

RecogFaceNetMobileNcnn::RecogFaceNetMobileNcnn(ConfigUP config)
    : RecognizerImpl(MAKE_UP(PathLoader)(config_const::mobilefacenet + ".param",
                                         config_const::mobilefacenet + ".bin"),
                     std::move(config))
{
    net = m_loader->load();
}

RecogFaceNetMobileNcnn::RecogFaceNetMobileNcnn(NcnnNetLoaderUP loader, ConfigUP config)
    : RecognizerImpl(std::move(loader), std::move(config))
{
    net = m_loader->load();
}
