//
// Created by haotaolai on 2021-01-22.
//
#include "recognizer.h"
#include "config.h"
#include "loader.h"

using namespace edet;

RecognizerImpl::RecognizerImpl()
    : m_loader(nullptr), m_config(MAKE_UP(Config)())
{
}

RecognizerImpl::RecognizerImpl(NcnnNetLoaderUP loader)
    : m_loader(std::move(loader)), m_config(MAKE_UP(Config)())
{
}

RecognizerImpl::RecognizerImpl(NcnnNetLoaderUP loader, ConfigUP config)
    : m_loader(std::move(loader)), m_config(std::move(config))
{
}

const ConfigUP& RecognizerImpl::config() const
{
    return m_config;
}

RecognizerImpl::~RecognizerImpl() = default;


EmbeddingSP Recognizer::compute(void* rgb, int width, int height)
{
    return p->compute(rgb, width, height);
}

Recognizer::Recognizer(RecognizerImplUP p)
    : p(std::move(p))
{
}
const ConfigUP& Recognizer::config() const
{
    return p->config();
}

Recognizer::~Recognizer() = default;
